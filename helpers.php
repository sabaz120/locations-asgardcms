<?php
use Modules\Locations\Entities\State;
use Modules\Locations\Entities\Parish;
use Modules\Locations\Entities\Municipality;

if (!function_exists('locations__getStates')) {

    function locations__getStates()
    {
        return State::all();
    }
}

if (!function_exists('locations__getParishes')) {

    function locations__getParishes()
    {
        return Parish::all();
    }
}

if (!function_exists('locations__getMunicipalities')) {

    function locations__getMunicipalities()
    {
        return Municipality::all();
    }
}
