<?php

return [
    'locations.states' => [
        'index' => 'locations::states.list resource',
        'create' => 'locations::states.create resource',
        'edit' => 'locations::states.edit resource',
        'destroy' => 'locations::states.destroy resource',
    ],
    'locations.municipalities' => [
        'index' => 'locations::municipalities.list resource',
        'create' => 'locations::municipalities.create resource',
        'edit' => 'locations::municipalities.edit resource',
        'destroy' => 'locations::municipalities.destroy resource',
    ],
    'locations.parishes' => [
        'index' => 'locations::parishes.list resource',
        'create' => 'locations::parishes.create resource',
        'edit' => 'locations::parishes.edit resource',
        'destroy' => 'locations::parishes.destroy resource',
    ],
// append



];
