<?php

namespace Modules\Locations\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterLocationsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        // $menu->group(trans('core::sidebar.content'), function (Group $group) {
            // $group->item(trans('locations::locations.title.locations'), function (Item $item) {
            //     $item->icon('fa fa-copy');
            //     $item->weight(10);
            //     $item->authorize(
            //          /* append */
            //     );
            //     $item->item(trans('locations::states.title.states'), function (Item $item) {
            //         $item->icon('fa fa-copy');
            //         $item->weight(0);
            //         $item->append('admin.locations.state.create');
            //         $item->route('admin.locations.state.index');
            //         $item->authorize(
            //             $this->auth->hasAccess('locations.states.index')
            //         );
            //     });
            //     $item->item(trans('locations::municipalities.title.municipalities'), function (Item $item) {
            //         $item->icon('fa fa-copy');
            //         $item->weight(0);
            //         $item->append('admin.locations.municipality.create');
            //         $item->route('admin.locations.municipality.index');
            //         $item->authorize(
            //             $this->auth->hasAccess('locations.municipalities.index')
            //         );
            //     });
            //     $item->item(trans('locations::parishes.title.parishes'), function (Item $item) {
            //         $item->icon('fa fa-copy');
            //         $item->weight(0);
            //         $item->append('admin.locations.parish.create');
            //         $item->route('admin.locations.parish.index');
            //         $item->authorize(
            //             $this->auth->hasAccess('locations.parishes.index')
            //         );
            //     });
// append



            // });
        // });

        return $menu;
    }
}
