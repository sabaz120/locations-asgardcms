<?php

namespace Modules\Locations\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Locations\Events\Handlers\RegisterLocationsSidebar;

class LocationsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterLocationsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('states', array_dot(trans('locations::states')));
            $event->load('municipalities', array_dot(trans('locations::municipalities')));
            $event->load('parishes', array_dot(trans('locations::parishes')));
            // append translations



        });
    }

    public function boot()
    {
        $this->publishConfig('locations', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Locations\Repositories\StateRepository',
            function () {
                $repository = new \Modules\Locations\Repositories\Eloquent\EloquentStateRepository(new \Modules\Locations\Entities\State());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Locations\Repositories\Cache\CacheStateDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Locations\Repositories\MunicipalityRepository',
            function () {
                $repository = new \Modules\Locations\Repositories\Eloquent\EloquentMunicipalityRepository(new \Modules\Locations\Entities\Municipality());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Locations\Repositories\Cache\CacheMunicipalityDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Locations\Repositories\ParishRepository',
            function () {
                $repository = new \Modules\Locations\Repositories\Eloquent\EloquentParishRepository(new \Modules\Locations\Entities\Parish());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Locations\Repositories\Cache\CacheParishDecorator($repository);
            }
        );
// add bindings



    }
}
