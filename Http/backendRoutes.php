<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/locations'], function (Router $router) {
    $router->bind('state', function ($id) {
        return app('Modules\Locations\Repositories\StateRepository')->find($id);
    });
    $router->get('states', [
        'as' => 'admin.locations.state.index',
        'uses' => 'StateController@index',
        'middleware' => 'can:locations.states.index'
    ]);
    $router->get('states/create', [
        'as' => 'admin.locations.state.create',
        'uses' => 'StateController@create',
        'middleware' => 'can:locations.states.create'
    ]);
    $router->post('states', [
        'as' => 'admin.locations.state.store',
        'uses' => 'StateController@store',
        'middleware' => 'can:locations.states.create'
    ]);
    $router->get('states/{state}/edit', [
        'as' => 'admin.locations.state.edit',
        'uses' => 'StateController@edit',
        'middleware' => 'can:locations.states.edit'
    ]);
    $router->put('states/{state}', [
        'as' => 'admin.locations.state.update',
        'uses' => 'StateController@update',
        'middleware' => 'can:locations.states.edit'
    ]);
    $router->delete('states/{state}', [
        'as' => 'admin.locations.state.destroy',
        'uses' => 'StateController@destroy',
        'middleware' => 'can:locations.states.destroy'
    ]);
    $router->bind('municipality', function ($id) {
        return app('Modules\Locations\Repositories\MunicipalityRepository')->find($id);
    });
    $router->get('municipalities', [
        'as' => 'admin.locations.municipality.index',
        'uses' => 'MunicipalityController@index',
        'middleware' => 'can:locations.municipalities.index'
    ]);
    $router->get('municipalities/create', [
        'as' => 'admin.locations.municipality.create',
        'uses' => 'MunicipalityController@create',
        'middleware' => 'can:locations.municipalities.create'
    ]);
    $router->post('municipalities', [
        'as' => 'admin.locations.municipality.store',
        'uses' => 'MunicipalityController@store',
        'middleware' => 'can:locations.municipalities.create'
    ]);
    $router->get('municipalities/{municipality}/edit', [
        'as' => 'admin.locations.municipality.edit',
        'uses' => 'MunicipalityController@edit',
        'middleware' => 'can:locations.municipalities.edit'
    ]);
    $router->put('municipalities/{municipality}', [
        'as' => 'admin.locations.municipality.update',
        'uses' => 'MunicipalityController@update',
        'middleware' => 'can:locations.municipalities.edit'
    ]);
    $router->delete('municipalities/{municipality}', [
        'as' => 'admin.locations.municipality.destroy',
        'uses' => 'MunicipalityController@destroy',
        'middleware' => 'can:locations.municipalities.destroy'
    ]);
    $router->bind('parish', function ($id) {
        return app('Modules\Locations\Repositories\ParishRepository')->find($id);
    });
    $router->get('parishes', [
        'as' => 'admin.locations.parish.index',
        'uses' => 'ParishController@index',
        'middleware' => 'can:locations.parishes.index'
    ]);
    $router->get('parishes/create', [
        'as' => 'admin.locations.parish.create',
        'uses' => 'ParishController@create',
        'middleware' => 'can:locations.parishes.create'
    ]);
    $router->post('parishes', [
        'as' => 'admin.locations.parish.store',
        'uses' => 'ParishController@store',
        'middleware' => 'can:locations.parishes.create'
    ]);
    $router->get('parishes/{parish}/edit', [
        'as' => 'admin.locations.parish.edit',
        'uses' => 'ParishController@edit',
        'middleware' => 'can:locations.parishes.edit'
    ]);
    $router->put('parishes/{parish}', [
        'as' => 'admin.locations.parish.update',
        'uses' => 'ParishController@update',
        'middleware' => 'can:locations.parishes.edit'
    ]);
    $router->delete('parishes/{parish}', [
        'as' => 'admin.locations.parish.destroy',
        'uses' => 'ParishController@destroy',
        'middleware' => 'can:locations.parishes.destroy'
    ]);
// append



});
