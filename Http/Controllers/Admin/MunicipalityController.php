<?php

namespace Modules\Locations\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Locations\Entities\Municipality;
use Modules\Locations\Http\Requests\CreateMunicipalityRequest;
use Modules\Locations\Http\Requests\UpdateMunicipalityRequest;
use Modules\Locations\Repositories\MunicipalityRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class MunicipalityController extends AdminBaseController
{
    /**
     * @var MunicipalityRepository
     */
    private $municipality;

    public function __construct(MunicipalityRepository $municipality)
    {
        parent::__construct();

        $this->municipality = $municipality;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$municipalities = $this->municipality->all();

        return view('locations::admin.municipalities.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('locations::admin.municipalities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMunicipalityRequest $request
     * @return Response
     */
    public function store(CreateMunicipalityRequest $request)
    {
        $this->municipality->create($request->all());

        return redirect()->route('admin.locations.municipality.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('locations::municipalities.title.municipalities')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Municipality $municipality
     * @return Response
     */
    public function edit(Municipality $municipality)
    {
        return view('locations::admin.municipalities.edit', compact('municipality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Municipality $municipality
     * @param  UpdateMunicipalityRequest $request
     * @return Response
     */
    public function update(Municipality $municipality, UpdateMunicipalityRequest $request)
    {
        $this->municipality->update($municipality, $request->all());

        return redirect()->route('admin.locations.municipality.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('locations::municipalities.title.municipalities')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Municipality $municipality
     * @return Response
     */
    public function destroy(Municipality $municipality)
    {
        $this->municipality->destroy($municipality);

        return redirect()->route('admin.locations.municipality.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('locations::municipalities.title.municipalities')]));
    }
}
