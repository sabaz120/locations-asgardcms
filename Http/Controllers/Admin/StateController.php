<?php

namespace Modules\Locations\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Locations\Entities\State;
use Modules\Locations\Http\Requests\CreateStateRequest;
use Modules\Locations\Http\Requests\UpdateStateRequest;
use Modules\Locations\Repositories\StateRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class StateController extends AdminBaseController
{
    /**
     * @var StateRepository
     */
    private $state;

    public function __construct(StateRepository $state)
    {
        parent::__construct();

        $this->state = $state;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$states = $this->state->all();

        return view('locations::admin.states.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('locations::admin.states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateStateRequest $request
     * @return Response
     */
    public function store(CreateStateRequest $request)
    {
        $this->state->create($request->all());

        return redirect()->route('admin.locations.state.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('locations::states.title.states')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  State $state
     * @return Response
     */
    public function edit(State $state)
    {
        return view('locations::admin.states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  State $state
     * @param  UpdateStateRequest $request
     * @return Response
     */
    public function update(State $state, UpdateStateRequest $request)
    {
        $this->state->update($state, $request->all());

        return redirect()->route('admin.locations.state.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('locations::states.title.states')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  State $state
     * @return Response
     */
    public function destroy(State $state)
    {
        $this->state->destroy($state);

        return redirect()->route('admin.locations.state.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('locations::states.title.states')]));
    }
}
