<?php

namespace Modules\Locations\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Locations\Entities\Parish;
use Modules\Locations\Http\Requests\CreateParishRequest;
use Modules\Locations\Http\Requests\UpdateParishRequest;
use Modules\Locations\Repositories\ParishRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ParishController extends AdminBaseController
{
    /**
     * @var ParishRepository
     */
    private $parish;

    public function __construct(ParishRepository $parish)
    {
        parent::__construct();

        $this->parish = $parish;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$parishes = $this->parish->all();

        return view('locations::admin.parishes.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('locations::admin.parishes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateParishRequest $request
     * @return Response
     */
    public function store(CreateParishRequest $request)
    {
        $this->parish->create($request->all());

        return redirect()->route('admin.locations.parish.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('locations::parishes.title.parishes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Parish $parish
     * @return Response
     */
    public function edit(Parish $parish)
    {
        return view('locations::admin.parishes.edit', compact('parish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Parish $parish
     * @param  UpdateParishRequest $request
     * @return Response
     */
    public function update(Parish $parish, UpdateParishRequest $request)
    {
        $this->parish->update($parish, $request->all());

        return redirect()->route('admin.locations.parish.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('locations::parishes.title.parishes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Parish $parish
     * @return Response
     */
    public function destroy(Parish $parish)
    {
        $this->parish->destroy($parish);

        return redirect()->route('admin.locations.parish.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('locations::parishes.title.parishes')]));
    }
}
