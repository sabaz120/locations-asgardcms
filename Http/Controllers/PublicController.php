<?php

namespace Modules\Locations\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Locations\Entities\State;
use Modules\Locations\Entities\City;
use Modules\Locations\Entities\Parish;
use Modules\Locations\Entities\Municipality;

class PublicController extends BasePublicController
{
  public function __construct()
  {
    parent::__construct();
  }

  public function states(){
    return response()->json(State::all(),200);
  }//states()

  public function municipalities(Request $request){
    $municipalities=Municipality::query();
    if(isset($request->state_id))
      $municipalities->where('state_id',$request->state_id);
    $municipalities=$municipalities->get();
    return response()->json($municipalities,200);
  }//municipalities()

  public function parishes(Request $request){
    $parishes=Parish::query();
    if(isset($request->municipality_id))
      $parishes->where('municipality_id',$request->municipality_id);
    return response()->json($parishes->get(),200);
  }//parishes()

  public function cities(Request $request){
    $cities=City::query();
    if(isset($request->state_id))
      $cities->where('state_id',$request->state_id);
    return response()->json($cities->get(),200);
  }//cities()

}
