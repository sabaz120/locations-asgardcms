<?php

use Illuminate\Routing\Router;
$locale = LaravelLocalization::setLocale() ?: App::getLocale();

$router->get('states', ['as' => 'locations.states', 'uses' => 'PublicController@states']);
$router->get('municipalities', ['as' => 'locations.municipalities', 'uses' => 'PublicController@municipalities']);
$router->get('parishes', ['as' => 'locations.parishes', 'uses' => 'PublicController@parishes']);
$router->get('cities', ['as' => 'locations.cities', 'uses' => 'PublicController@cities']);
