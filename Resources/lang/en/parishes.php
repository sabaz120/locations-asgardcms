<?php

return [
    'list resource' => 'List parishes',
    'create resource' => 'Create parishes',
    'edit resource' => 'Edit parishes',
    'destroy resource' => 'Destroy parishes',
    'title' => [
        'parishes' => 'Parish',
        'create parish' => 'Create a parish',
        'edit parish' => 'Edit a parish',
    ],
    'button' => [
        'create parish' => 'Create a parish',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
