<?php

return [
    'list resource' => 'List municipalities',
    'create resource' => 'Create municipalities',
    'edit resource' => 'Edit municipalities',
    'destroy resource' => 'Destroy municipalities',
    'title' => [
        'municipalities' => 'Municipality',
        'create municipality' => 'Create a municipality',
        'edit municipality' => 'Edit a municipality',
    ],
    'button' => [
        'create municipality' => 'Create a municipality',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
