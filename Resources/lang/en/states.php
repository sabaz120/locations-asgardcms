<?php

return [
    'list resource' => 'List states',
    'create resource' => 'Create states',
    'edit resource' => 'Edit states',
    'destroy resource' => 'Destroy states',
    'title' => [
        'states' => 'State',
        'create state' => 'Create a state',
        'edit state' => 'Edit a state',
    ],
    'button' => [
        'create state' => 'Create a state',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
