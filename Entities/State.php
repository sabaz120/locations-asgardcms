<?php

namespace Modules\Locations\Entities;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $table = 'locations__states';
    protected $fillable = [
      'name',
      'iso_3166-2'
    ];
}
