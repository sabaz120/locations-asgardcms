<?php

namespace Modules\Locations\Entities;

use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{

    protected $table = 'locations__parishes';
    protected $fillable = [
      'municipality_id',
      'name'
    ];

    public function municipality(){
      return $this->belongsTo('Modules\Locations\Entities\Municipality','municipality_id');
    }
}
