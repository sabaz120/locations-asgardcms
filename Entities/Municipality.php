<?php

namespace Modules\Locations\Entities;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{

    protected $table = 'locations__municipalities';
    protected $fillable = [
      'state_id',
      'name'
    ];

    public function state(){
      return $this->belongsTo('Modules\Locations\Entities\State','state_id');
    }
}
