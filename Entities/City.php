<?php

namespace Modules\Locations\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $table = 'locations__cities';
    protected $fillable = [
      'state_id',
      'city',
      'capital'
    ];
}
