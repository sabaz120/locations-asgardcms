<?php

namespace Modules\Locations\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface ParishRepository extends BaseRepository
{
}
