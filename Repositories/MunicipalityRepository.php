<?php

namespace Modules\Locations\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface MunicipalityRepository extends BaseRepository
{
}
