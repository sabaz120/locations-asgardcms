<?php

namespace Modules\Locations\Repositories\Eloquent;

use Modules\Locations\Repositories\StateRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentStateRepository extends EloquentBaseRepository implements StateRepository
{
}
