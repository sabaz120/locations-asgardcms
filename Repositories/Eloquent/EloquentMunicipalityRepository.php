<?php

namespace Modules\Locations\Repositories\Eloquent;

use Modules\Locations\Repositories\MunicipalityRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMunicipalityRepository extends EloquentBaseRepository implements MunicipalityRepository
{
}
