<?php

namespace Modules\Locations\Repositories\Eloquent;

use Modules\Locations\Repositories\ParishRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentParishRepository extends EloquentBaseRepository implements ParishRepository
{
}
