<?php

namespace Modules\Locations\Repositories\Cache;

use Modules\Locations\Repositories\ParishRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheParishDecorator extends BaseCacheDecorator implements ParishRepository
{
    public function __construct(ParishRepository $parish)
    {
        parent::__construct();
        $this->entityName = 'locations.parishes';
        $this->repository = $parish;
    }
}
