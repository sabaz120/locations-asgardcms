<?php

namespace Modules\Locations\Repositories\Cache;

use Modules\Locations\Repositories\StateRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheStateDecorator extends BaseCacheDecorator implements StateRepository
{
    public function __construct(StateRepository $state)
    {
        parent::__construct();
        $this->entityName = 'locations.states';
        $this->repository = $state;
    }
}
