<?php

namespace Modules\Locations\Repositories\Cache;

use Modules\Locations\Repositories\MunicipalityRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMunicipalityDecorator extends BaseCacheDecorator implements MunicipalityRepository
{
    public function __construct(MunicipalityRepository $municipality)
    {
        parent::__construct();
        $this->entityName = 'locations.municipalities';
        $this->repository = $municipality;
    }
}
